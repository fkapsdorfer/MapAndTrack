/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license GPLv3
 * @file ...
 */

'use strict';

(function(app){
	app.milestones = {

		/**
		 * Is time milestone.
		 */
		isTimeMs: function(/*[vi1 [, vi2, ...]*/) {
			for (var i = 0; i < arguments.length; i++) {
				var t = app.data.track[arguments[i]];
				if (!t || t.timeDelta == null) {
					return false;
				}
			}
			return true;
		},

		/**
		 * Is elevation milestone.
		 */
		isEleMs: function(/*[vi1 [, vi2, ...]*/) {
			for (var i = 0; i < arguments.length; i++) {
				var t = app.data.track[arguments[i]];
				if (!t || t.ele == null) {
					return false;
				}
			}
			return true;
		},


		/**
		 * @param {Number} vi1 	Vertex index
		 * @param {Number} [vi2]
		 * @return {Number} 	The total distance
		 */
		computeDeltaDistance: function(vi1, vi2) {
			var t 	= app.data.track;
			var p 	= app.polyline.getPath();
			var dis = 0;

			// if to find the nearest time milestone
			if (vi2 == null) {
				vi2 = app.milestones.findNearestPrevTimeMs(vi1);
			}

			// switch
			if (vi2 < vi1) {
				var temp 	= vi1;
				vi1 		= vi2;
				vi2 		= temp;
			}

			for (var i = vi1; i < vi2; i++) { // note i+1
				dis += google.maps.geometry.spherical.computeDistanceBetween(p.getAt(i), p.getAt(i+1));
			}

			return dis;
		},

		computeTotalDistance: function(vi) {
			return app.milestones.computeDeltaDistance(0, vi);
		},


		/**
		 * @return {Number|undefined} 	Vertex index
		 */
		findNearestPrevTimeMs: function(vi) {
			var t = app.data.track;
			for (var i=vi-1; 0<=i; i--) {
				if (app.milestones.isTimeMs(i)) {
					return i;
				}
			}
		},

		/**
		 * @return {Number|undefined} 	Vertex index
		 */
		findNearestNextTimeMs: function(vi) {
			var t = app.data.track;
			for (var i=vi+1; i<t.length; i++) {
				if (app.milestones.isTimeMs(i)) {
					return i;
				}
			}
		},


		/**
		 * The both vertices must be time milestones.
		 *
		 * @param {Number} [vi1]
		 * @param {Number} [vi2]
		 * @return {Number|null} 	The time in milliseconds
		 */
		computeDeltaTime: function(vi1, vi2) {
			// if to find the nearest time milestone
			if (vi2 == null) {
				vi2 = app.milestones.findNearestPrevTimeMs(vi1);
			}
			if (!app.milestones.isTimeMs(vi1, vi2)) {
				return null;
			}

			var time 	= 0;

			// switch
			if (vi2 < vi1) {
				var temp 	= vi1;
				vi1 		= vi2;
				vi2 		= temp;
			}

			for (var i = vi1+1; i <= vi2; i++) {
				if (app.milestones.isTimeMs(i)) {
					time += app.data.track[i].timeDelta;
				}
			}

			return time;
		},

		computeTotalTime: function(vi) {
			return app.milestones.computeDeltaTime(0, vi);
		},


		/**
		 * Both of the vertices must be time milestones.
		 *
		 * @param {Number} vi1
		 * @param {Number} [vi2]
		 * @param {String} [unit]
		 * @return {Number|null}
		 */
		computeAvrgSpeed: function(vi1, vi2, unit) {
			var ms 			= app.milestones;
			var multiply 	= unit === 'kmph'
				? 3.6
				: 1;
			var distance 	= ms.computeDeltaDistance(vi1, vi2);
			var time 		= ms.computeDeltaTime(vi1, vi2);

			if (distance && time) {
				return distance / (time / 1000) * multiply;
			}
		},


		/**
		 * @return {Number|undefined} 	Vertex index
		 */
		findNearestPrevEleMs: function(vi) {
			var t = app.data.track;
			for (var i=vi-1; 0<=i; i--) {
				if (app.milestones.isEleMs(i)) {
					return i;
				}
			}
		},

		/**
		 * @return {Number|undefined} 	Vertex index
		 */
		findNearestNextEleMs: function(vi) {
			var t = app.data.track;
			for (var i=vi+1; i<t.length; i++) {
				if (app.milestones.isEleMs(i)) {
					return i;
				}
			}
		},


		/**
		 * This is just a single (sum) delta.
		 * Both of the vertices must be elevation milestones.
		 *
		 * @param {Number} vi1
		 * @param {Number} [vi2]
		 * @return {Number|null}
		 */
		computeDeltaElevation: function(vi1 ,vi2) {
			// if to find the nearest time milestone
			if (vi2 == null) {
				vi2 = app.milestones.findNearestPrevTimeMs(vi1);
			}
			if (!app.milestones.isEleMs(vi1, vi2)) {
				return null;
			}

			var t = app.data.track;

			// switch
			if (vi2 < vi1) {
				var temp 	= vi1;
				vi1 		= vi2;
				vi2 		= temp;
			}

			return t[vi2].ele - t[vi1].ele;
		},

		/**
		 * Both of the vertices must be elevation milestones.
		 *
		 * @param {Number} vi1
		 * @param {Number} [vi2]
		 * @return {Object} { desc: {Number}, asce: {Number} } (descending, ascending) or an empty object
		 */
		computeCumulativeElevation: function(vi1, vi2) {
			var ms = app.milestones;
			// if to find the nearest time milestone
			if (vi2 == null) {
				vi2 = ms.findNearestPrevTimeMs(vi1);
			}
			if (!ms.isEleMs(vi1, vi2)) {
				return {};
			}

			var t 			= app.data.track;
			var desc 		= 0;
			var asce 		= 0;
			var delta;
			var nextI; 	// always greater than prevI
			var prevI; 				// = (nextI - (i>=1))

			// switch
			if (vi2 < vi1) {
				var temp 	= vi1;
				vi1 		= vi2;
				vi2 		= temp;
			}

			nextI = vi2;

			// sum the individual deltas
			do {
				prevI = ms.findNearestPrevEleMs(nextI);
				if (prevI == null) {
					break;
				}

				delta = t[nextI].ele - t[prevI].ele;

				nextI = prevI;

				if (delta < 0) {
					desc -= delta;
				} else {
					asce += delta;
				}
			} while (vi1 < prevI);

			return { desc: desc, asce: asce };
		}

	}
})(
	typeof window.app === 'object'
		? window.app
		: window.app = {}
)
