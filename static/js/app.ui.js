/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license GPLv3
 * @file ...
 */

'use strict';

(function(app){

	var ui 			= app.ui;
	var dom 		= app.dom;
	var util 		= app.util;
	var qs 			= util.dom.qs;
	var on 			= util.evt.on;
	var one 		= util.evt.one;
	var hdlr 		= app._handlers.dashboard;



	ui.initSelectors = function() {
		var d 			= app.dom;
		var temp;

		d.home 			= qs('#home-container');
		d.uploader 		= qs('#uploader');
		d.map 			= qs('#map-container');
		d.loading 		= qs('#blockUI-loading');

		d.promptEl 		= qs('#prompt');
		d.prompt 		= {
			title 	: qs(d.promptEl, '.title'),
			msg 	: qs(d.promptEl, '.msg'),
			btn 	: qs(d.promptEl, 'button'),
			textarea: qs(d.promptEl, 'textarea')
		}

		d.description 	= qs('#description');
		d.descriptions 	= {
			// filled by the following loop, e.g.:
			// modelProp: el,
			// ...
		};
		var els = d.description.querySelectorAll('[data-model]');
		for (var i = 0; i < els.length; i++) {
			var el = els[i];
			d.descriptions[el.dataset.model] = el;
		}

		d.uploaderFile 	= qs(d.uploader	, 'input[type="file"]');
		d.homeNewBtn 	= qs(d.home		, '.create-new');

		d.dashboard 		= {};
		var dbo 			= d.dashboard;
		var dbe 			= qs('#dashboard');
		d.dashboardEl 		= dbe;

		dbo.vertexEditor 	= qs(dbe, '#vertex-editor');
		dbo.vertexEditors 	= dbo.vertexEditor.querySelectorAll('input[type="text"], textarea');

		dbo.filter 			= qs(dbe, '#filter')
		dbo.filters 		= dbo.filter.querySelectorAll('input[type="checkbox"], select');
		dbo.export 			= qs(dbe, '#export');
		dbo.exportTypes 	= dbo.export.querySelectorAll('input[type="radio"][name="export-type"]');
		dbo.exportOptionsEl = qs(dbo.export, '#export-options');
		dbo.exportOptions 	= function(type) { return dbo.exportOptionsEl.querySelectorAll('[data-for="' + type + '"] input[type="checkbox"') };
		dbo.exportBtn 		= qs(dbo.export, '#export-btn');

	}



	ui.blockUI = function(loading) {
		var ac = util.dom.addClass;

		ac(document.body, 'blockUI');
		if (loading) {
			ac(dom.loading, 'shown')
		}
	}

	ui.unblockUI = function() {
		var rc = util.dom.removeClass;

		rc(document.body, 'blockUI');
		rc(dom.loading, 'shown');
	}



	ui.prompt = function(title, msg, cb) {
		var po 	= dom.prompt;

		po.title.innerHTML 	= title || '';
		po.msg.innerHTML 	= msg || '';

		ui.blockUI();
		util.dom.addClass(dom.promptEl, 'shown');

		one(po.btn, 'click', function(){
			util.dom.removeClass(dom.promptEl, 'shown');
			ui.unblockUI();
			if (cb instanceof Function) {
				cb();
			}
		});
	}

	ui.promptRawData = function(msg, raw, cb) {
		var p 	= dom.promptEl;
		var po 	= dom.prompt;
		var ac 	= util.dom.addClass;
		var rc 	= util.dom.removeClass;

		po.title.innerHTML 	= '';
		po.msg.innerHTML 	= msg || '';

		ui.blockUI();
		ac(p, 'raw-data');
		ac(p, 'shown');

		po.textarea.value = raw;
		po.textarea.click();

		one(btn, 'click', function(){
			rc(p, 'raw-data');
			rc(p, 'shown');
			ui.unblockUI();
			if (cb instanceof Function) {
				cb();
			}
		});
	}


	ui.descriptionShown = function() {
		var d = dom.description;
		if (util.dom.hasClass(d, 'shown')) {
			return true;
		} else {
			return false;
		}
	}

	ui.toggleDescription = function(i) {
		if (
			i == null ||
			(
				ui.descriptionShown() &&
				app._currentVertex === i
			)
		) {
			ui.hideDescription();
		} else {
			ui.showDescription(i);
		}
	}

	ui.showDescription = function(i) {
		var t 	= app.data.track[i];
		var de 	= dom.description;
		var d 	= dom.descriptions;
		var ms  = app.milestones;
		var f 	= app.filter;
		var i2 	= app._currentVertex;
		var temp= null;

		if (!app._vertexLocked) {
			i2 = null;
		}


		if (f.coords) {
			d.lat.innerHTML = t.lat.toFixed(6);
			d.lng.innerHTML = t.lng.toFixed(6);
		} else {
			d.lat.innerHTML = '-';
			d.lng.innerHTML = '-';
		}

		temp = null;
		if (f.disTotal) {
			temp = ms.computeTotalDistance(i, i2);
		}
		d.disTotal.innerHTML = temp != null
			? temp.toFixed(2) + ' m'
			: '-';

		temp = null;
		if (f.disDelta) {
			temp = ms.computeDeltaDistance(i, i2);
		}
		d.disDelta.innerHTML = temp != null
			? temp.toFixed(2) + ' m'
			: '-';

		temp = null;
		if (f.timeTotal) {
			temp = ms.computeTotalTime(i, i2);
		}
		d.timeTotal.innerHTML = temp != null
			? util.time.msToStr(temp)
			: '-';

		temp = null;
		if (f.timeDelta) {
			temp = ms.computeDeltaTime(i, i2);
		}
		d.timeDelta.innerHTML = temp != null
			? util.time.msToStr(temp)
			: '-';

		temp = null;
		if (f.avrgSpeed) {
			temp = ms.computeAvrgSpeed(i, i2, f.speedUnit);
		}
		d.avrgSpeed.innerHTML = temp != null
			? temp.toFixed(2) + ' ' + f.speedUnit
			: '-';

		if (f.ele) {
			d.ele.innerHTML = t.ele != null
				? t.ele.toFixed(2) + ' m'
				: '-';
		} else {
			d.ele.innerHTML = '-';
		}

		temp = null;
		if (f.eleDelta) {
			temp = ms.computeDeltaElevation(i, i2);
		}
		d.eleDelta.innerHTML = temp != null
			? temp.toFixed(2) + ' m'
			: '-';

		temp = {};
		if (f.eleCumulative) {
			temp = ms.computeCumulativeElevation(i, i2);
		}
		d.eleCumulativeAsce.innerHTML = temp.asce != null
			? temp.asce.toFixed(2) + ' m'
			: '-';
		d.eleCumulativeDesc.innerHTML = temp.desc != null
			? temp.desc.toFixed(2) + ' m'
			: '-';

		if (f.note) {
			d.note.innerHTML = t.note || '-';
		} else {
			d.note.innerHTML = '-';
		}


		de.style.display = 'inherit';
		util.dom.addClass(de, 'shown');
	}

	ui.hideDescription = function() {
		var d = dom.description;
		one(d,  'transitionend', function(){
			d.style.display = 'none';
		});
		util.dom.removeClass(d, 'shown');
	}



	ui.updateDashboard = function() {
		var editors = dom.dashboard.vertexEditors;

		for (var i = 0; i < editors.length; i++) {
			util.mvc.updateView(editors[i]);
		}
	}

	ui.initDashboard = function() {
		var t 			= app.data.track;
		var h 			= app._handlers.dashboard;
		var dbo 		= dom.dashboard;
		var dbe 		= dom.dashboardEl;
		var vEditors 	= dbo.vertexEditors;
		var filters 	= dbo.filters;
		var bar 		= qs(dbe, '.bar');


		dbe.style.display = 'block';


		// show / hide dashboard
		on(bar, 'click', function(){
			util.dom.toggleClass(dbe, 'shown');
		});


		// vertex editors - listeners
		// iterating over vertex editor inputs (Element(s))
		for (var i = 0; i < vEditors.length; i++) {
			var el = vEditors[i];
			// all elements are text inputs or textarea
			on(el, 'change', hdlr.changeVertexEditor);
		}


		// filters -  listeners, default values
		// iterating over filter inputs (Element(s))
		for (var i = 0; i < filters.length; i++) {
			var el 	= filters[i];
			var val = util.obj.getPropVal(el.dataset.model, app);

			if (el.nodeName === 'INPUT') {	// checkboxes
				el.checked = val;
			} else { 						// select lists
				el.value = val;
			}

			on(el, 'change', hdlr.changeFilter);
		}

		// export radios - do not have model, use ui.getExportType()
		var els = dbo.exportTypes;
		// listeners
		for (var i = 0; i < els.length; i++) {
			on(els[i], 'change', hdlr.changeExportType);
		}

		// export button
		on(dbo.exportBtn, 'click', hdlr.clickExportBtn);

	}

	ui.getExportType = function() {
		var els = dom.dashboard.exportTypes;
		for (var i = 0; i < els.length; i++) {
			var el = els[i];
			if (el.checked) {
				return el.value;
			}
		}
	}

	ui.showExportOptions = function(exType) {
		var dbo = dom.dashboard;

		// reference to what is selected
		dbo.exportOptionsEl.dataset.for = exType;

		// show export button
		dbo.exportBtn.removeAttribute('hidden');
	}

	/**
	 * @return {Object} 	{ optName: val, ... }
	 */
	ui.getExportOptions = function(type) {
		type 		= type || ui.getExportType();
		var els 	= dom.dashboard.exportOptions(type);
		var options = {};

		for (var i = 0; i < els.length; i++) {
			options[els[i].className] = els[i].checked;
		}

		return options;
	}



	hdlr.changeVertexEditor = function() {
		util.mvc.updateModel(this);
	}

	hdlr.changeFilter = function(evt) {
		util.mvc.updateModel(this);
	}

	hdlr.changeExportType = function() {
		var exType 	= this.value;
		ui.showExportOptions(exType);
	}

	hdlr.clickExportBtn = function() {
		var exType 	= ui.getExportType();
		var opts 	= ui.getExportOptions(exType);

		if (exType == null) {
			return;
		}

		app.export(exType, opts);
	}


})(window.app)
