/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license GPLv3
 * @file ...
 */

'use strict';

(function(app){
	var qs 	= app.util.dom.qs;
	var on 	= app.util.evt.on;
	var one = app.util.evt.one;
	var ui = app.ui;


	window.addEventListener('load', function() {
		ui.initSelectors();
		app.initUI();
	});


	app.initUI = function () {
		// "crete new project" button
		app.dom.homeNewBtn.addEventListener('click', app.createNewProject);

		app.uploader.initUploader();
	}






	app.createNewProject = function() {

		if (navigator.geolocation) {
			// try to get position
			navigator.geolocation.getCurrentPosition(_done, _done, { timeout: 1000 });
		} else {
			_done();
		}

		function _done (res) {
			if (!res.coords) {
				//console.info('Cannot get the approximate current position.', res);
			} else {
				app.data.mapOpts.center = {
					lat: res.coords.latitude,
					lng: res.coords.longitude
				};
			}

			app._handlers.dataReady();
		}
	}


	app.initMap = function() {
		var o 		= app.data.mapOpts;
		var mapEl 	= app.dom.map;
		var pline 	= app.polyline;
		var path 	= pline.getPath();
		var ta 		= app.data.track;
		var center 	= new google.maps.LatLng(o.center.lat, o.center.lng);

		var map = app.map
				= new google.maps.Map(mapEl, {
			center			: center,
			zoom			: o.zoom,
			disableDefaultUI: true,
			mapTypeId		: google.maps.MapTypeId[o.mapTypeId],
		});

		// fix the issue when the map does not show up unitil resize
		google.maps.event.addListenerOnce(map, 'idle', function() {
			google.maps.event.trigger(map, 'resize');
			map.setCenter(center);
			map.setZoom(o.zoom);
		});

		// set vertices
		for (var i = 0; i < ta.length; i++) {
			var t = ta[i];
			path.push(new google.maps.LatLng(t.lat, t.lng));
		}

		pline.setMap(map);
		app._curVertexMarker.setMap(map);

		mapEl.style.display = 'block';


		var mapH = app._handlers.map;

		google.maps.event.addListener(map, 'click', mapH.clickMap);
		google.maps.event.addListener(pline, 'click', mapH.clickTrackVertex);
		google.maps.event.addListener(pline, 'dblclick', mapH.dblclickTrackVertex);
		google.maps.event.addListener(pline, 'mouseover', mapH.mouseoverTrackVertex);
		google.maps.event.addListener(pline, 'mouseout', mapH.mouseoutTrackVertex);


		var dataH = app._handlers.dataTrack;

		// MVC - detect changes in the model
		google.maps.event.addListener(path, 'set_at', dataH.setPathVertex);
		google.maps.event.addListener(path, 'insert_at', dataH.insertPathVertex);
		google.maps.event.addListener(path, 'remove_at', dataH.removePathVertex);
	}






	app._handlers.dataReady = function() {
		app.dom.home.style.display 	= 'none';

		app.initMap();
		ui.initDashboard();
	}

	app._handlers.dataTrack = {
		setPathVertex: function(i) {
			var v = app.polyline.getPath().getAt(i);
			var t = app.data.track[i];
			t.lat = v.lat();
			t.lng = v.lng();

			// if the current vertex changed
			app.setCurrentVertex(app._currentVertex);
			ui.updateDashboard();
		},
		insertPathVertex: function(i) {
			var p = app.polyline.getPath()
			var v = p.getAt(i);
			app.data.track.splice(i, 0, {
				lat: v.lat(),
				lng: v.lng()
			});

			if (app.filter.disableCreation) {
				// note that the vertex must be first added to app.data.track
				// this is done on purpose
				p.removeAt(i);
			} else {
				if (i <= app._currentVertex) {
					app._currentVertex++;
				}
			}
		},
		removePathVertex: function(i) {
			app.data.track.splice(i, 1);
			app.unsetCurrentVertex();
			ui.updateDashboard();
		}
	}

	app._handlers.map = {
		clickMap: function(evt) {
			if (ui.descriptionShown()) {
				// hide the description
				ui.hideDescription();
			} else if (!app._vertexLocked && !app.filter.disableCreation) {
				// create a new vertex
				var v = evt.latLng;
				app.polyline.getPath().push(v);
			}
		},
		dblclickTrackVertex: function(evt) {
			var i = evt.vertex;

			if (i == null || app.filter.disableRemoval) {
				return;
			}
			// remove the vertex
			app.polyline.getPath().removeAt(i);
		},
		clickTrackVertex: function(evt) {
			var i = evt.vertex;

			if (i == null) {
				return;
			}
			// toggle the description
			ui.toggleDescription(i);

			if (!app._vertexLocked) {
				// set as the current vertex
				app.setCurrentVertex(i);
				ui.updateDashboard();
			}
		},
		mouseoverTrackVertex: function(evt) {
			if (evt.vertex == null || !app.filter.enableHover) {
				return;
			}
			ui.showDescription(evt.vertex);
		},
		mouseoutTrackVertex: function() {
			if (!app.filter.enableHover) {
				return;
			}
			ui.hideDescription();
		}
	};



	app.mvcM2VSetters = {
		vertexEditor: function(el) {
			var val;
			var i 			= app._currentVertex;
			var prop 		= el.dataset.model;
			var disable 	= i == null
				? true
				: false;

			val = i != null
				? app.data.track[i][prop] || ''
				: '';

			if (prop === 'timeDelta' && val !== '') {
				val = app.util.time.msToStr(val);
			}

			el.disabled = disable;
			el.value 	= val;
		}
	}
	app.mvcV2MSetters = {
		vertexLocked: function(val, el) {
			var res;
			if (val) {
				res = app._lockVertex();
			} else {
				res = app._unlockVertex();
			}
			// if unable to lock (if no current vertex set)
			if (!res) {
				app.util.mvc.updateView(el);
			}
		},
		vertexEditor: {
			timeDelta: function(v) {
				app._setVertexDeltaTime(app._currentVertex, v);
			},
			ele: function(v) {
				app._setVertexEle(app._currentVertex, v);
			},
			note: function(v) {
				app._setVertexNote(app._currentVertex, v);
			}
		}
	};



	/**
	 * _ prefix denotes that the method does not update the (corresponding) view except the map view
	 */
	(function(){
		var marker = app._curVertexMarker;

		app.setCurrentVertex = function(i) {
			if (i == null) {
				return app.unsetCurrentVertex();
			} else {
				app._currentVertex = i;
				marker.setPosition(app.polyline.getPath().getAt(i));
				marker.setMap(app.map);
				if (!app._vertexLocked) {
					marker.setTitle('current');
					marker.setLabel('C');

					return true;
				}
			}
		}
		app.unsetCurrentVertex = function() {
			app.unlockVertex();
			app._currentVertex = null;
			marker.setMap(null);

			return true;
		}

		/**
		 * @return {Boolean} If able to lock
		 */
		app._lockVertex = function() {
			if (app._currentVertex == null) {
				// unable to set
				return false;
			} else {
				if (!app._vertexLocked) {
					app._vertexLocked = true;
					marker.setTitle('current locked');
					marker.setLabel('L');

					return true;
				}
			}
		}
		app._unlockVertex = function() {
			if (app._vertexLocked) {
				app._vertexLocked = false;
				marker.setTitle('current');
				marker.setLabel('C');
			}
		}
		app.unlockVertex = function() {
			app._unlockVertex();
			app.util.mvc.updateView(app.dom.dashboard.filter, '_vertexLocked');
		}

		app._setVertexDeltaTime = function(i, v) {
			var t = app.data.track[i];

			if (i !== null) {
				var time = app.util.time.strToMs(v);
				if (isNaN(time) || (time === 0 && i !== 0)) {
					// if to remove
					delete t.timeDelta;
				} else {
					t.timeDelta = time;
				}
			}
		}
		app._setVertexEle = function(i, v) {
			var t = app.data.track[i];

			if (i !== null) {
				if (isNaN(v)) {
					// if to remove
					delete tp.ele;
				} else {
					t.ele = parseFloat(v);
				}
			}
		}
		app._setVertexNote = function(i, v) {
			var t = app.data.track[i];

			if (i !== null) {
				if (v == '') {
					delete t.note;
				} else {
					t.note = v;
				}
			}
		}
	})();


	/**
	 * Updates app.data.mapOpts according to the current map settings (view).
	 */
	app.updateMapOpts = function() {
		var m = app.map;
		var o = app.data.mapOpts;
		var c = m.getCenter();

		o.zoom = m.getZoom();
		o.center = { lat: c.lat(), lng: c.lng() };
		o.mapTypeId = m.getMapTypeId().toUpperCase();
	}


	app.export = function(exType, opts) {
		var exObj = { mapOpts: {}, track: {} };
		var d = app.data;

		if (exType == null) {
			return;
		}

		app.updateMapOpts();

		if (exType === '.ts.json') {
			exObj = d;
		} else if (exType === '.tl.json') {
			exObj = d;

			if (!opts['include-notes']) {
				// to clone the object
				exObj = JSON.stringify(exObj);
				exObj = JSON.parse(exObj);
				var t = exObj.track;

				for (var i = 0; i < t.length; i++) {
					delete t[i].note;
				}
			}
		} else if (exType === '.tf.json') {
			if (opts['include-map-settings']) {
				exObj.mapOpts = d.mapOpts;
			}
			for (var i = 0; i < d.track.length; i++) {
				var  t = d.track[i];
				if (t.timeDelta != null) {
					// transform array to objects
					// so it's possible to determine the correct vertex index to import
					exObj.track['' + i] = {
						note: 		t.note,
						timeDelta: 	t.timeDelta
					}
				}
			}
		}


		d = JSON.stringify(exObj);

		var res = app.util.download(
			d,
			'new-map' + exType,
			'application/javascript'
		);

		if (!res) {
			app.ex.downloadNotSupported(exType, d);
		}
	}

	/**
	 * @prop {Object} data
	 * @prop {Object} data.ts 	.ts.json format
	 * @prop {Object} data.tl 	.tl.json format
	 * @prop {Object} data.tf 	.tf.json format
	 * @prop {Number} len 		Number of uploaded files
	 */
	app.import = function(data, len) {
		var merge = app.util.obj.merge;

		if (len === 1 && data.tf) {
			app.ex.invalidFileVariation();
		}

		if (data.ts) {
			merge(app.data, data.ts);
		} else {
			merge(app.data, data.tl);
			if (data.tf) {
				var t = data.tf.track;
				// options
				merge(app.data.mapOpts, data.tf.mapOpts);
				// vertices
				for (var vi in t) {
					if (t.hasOwnProperty(vi)) {
						merge(app.data.track[parseInt(vi)], t[vi]);
					}
				}
			}
		}

		app._handlers.dataReady();
	}

})(window.app);
