/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license GPLv3
 * @file ...
 */

'use strict';

(function(){

	var app = {

		CONST: {
			exportTypes: { // do not change, just a reference
				ts_json: '.ts.json',
				tl_json: '.tl.json',
				tf_json: '.tf.json'
			},
			ModelLocked: function() {} // an instance is returned if app.mvcInputFilters.aFilter() cannot set a model value
		},

		util: {
			dom: {},
			evt: {},
			obj: {},
			str: {},
			time: {},
			mvc: {}
		},
		ui: {},
		ex: {},
		uploader: {},
		milestones: {},
		_handlers: {
			// dataTrack: {},
			// map: {},
			dashboard: {}
		},
		// mvcSetters: {},
		// mvcGetters: {},

		data: { 	// default
			mapOpts: {
				center: {
					lat: 35,
					lng: 35
				},
				zoom: 3,
				mapTypeId: 'ROADMAP'
			},
			track: []
		},

		filter: {
			enableHover: false,
			disableRemoval: false,
			disableCreation: false,

			coords: false,
			disTotal: true,
			disDelta: true,
			timeTotal: true,
			timeDelta: true,
			avrgSpeed: true,
			speedUnit: 'kmph', // , mps
			ele: true,
			eleDelta: true,
			eleCumulative: true,
			note: true
		},

		dom 		: {},

		map 		: null/*new google.maps.Map()*/,
		polyline 	: new google.maps.Polyline({
			path			: [],
			geodesic		: true,
			draggable		: false,
			editable		: true,
			strokeColor		: '#FF0000',
			strokeOpacity	: 0.7,
			strokeWeight	: 2
		}),
		_curVertexMarker: new google.maps.Marker({
			title: 'current',
			label: 'C'
		}),

		_currentVertex 	: null,
		_vertexLocked 	: false
	}


	// export
	window.app = app;

})()
