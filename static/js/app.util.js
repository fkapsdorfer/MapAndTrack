/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license GPLv3
 * @file ...
 */

'use strict';

(function(app){

	var util = app.util;



	/**
	 * Prompt downloading of a file with given data.
	 *
	 * @return {Boolean} if success
	 */
	util.download = function(data, fname, mime) {
		var URL 	= window.URL;
		var Blob 	= window.Blob;

		if (!URL) {
			URL = window.webkitURL;

			if (!URL) {
				return false;
			}
		}

		if (!Blob) {
			Blob = window.WebKitBlob;

			if (!Blob) {
				return false;
			}
		}


		var blob = new Blob([data], { type: mime });
		var url = URL.createObjectURL(blob);

		// simulate a button click to "force" the download
		var el = document.createElement('a');
		el.href = url;
		el.download = fname;
		document.body.appendChild(el);
		el.click();
		el.remove();

		setTimeout(function(){
			// free the memory
			URL.revokeObjectURL(url);
		}, 100);

		return true;
	}



	/**
	 * Selects a single element.
	 * The element can be queried from the whole document or relatively from a specific (parent) element.
	 *
	 * @param {Element|String} 	elOrStr 	A DOM element or a query selector
	 * @param {String} 			[selStr] 	A query selector
	 */
	util.dom.qs = function(elOrStr, selStr) {
		if (typeof elOrStr === 'string') {
			return document.querySelector(elOrStr);
		} else if (elOrStr instanceof Element && typeof selStr === 'string') {
			return elOrStr.querySelector(selStr);
		}
	}

	util.dom.hasClass = function(el, cls) {
		var c 		= el.className;
		var regex 	= new RegExp('( ' + cls + ')|(' + cls + ' )|(^' + cls + '$)');

		return !!c.match(regex);
	}

	util.dom.toggleClass = function(el, cls) {
		var d = util.dom;

		if (d.hasClass(el, cls)) {
			d.removeClass(el, cls);
		} else {
			d.addClass(el, cls);
		}
	}

	util.dom.addClass = function(el, cAdd) {
		var c = el.className;

		if (!util.dom.hasClass(el, cAdd)) {
			el.className = ''
				+ c
				+ (0 < c.length ? ' ' : '')
				+ cAdd;
		}
	}

	util.dom.removeClass =	function(el, cRem) {
		var c 		= el.className;
		var regex 	= new RegExp('( ' + cRem + ')|(' + cRem + ' )|(^' + cRem + '$)');

		el.className = c.replace(regex, '');
	}



	/**
	 * Adds event listener.
	 *
	 * @param {EventTarget} 	obj 	An object that implements EventTarget interface
	 * @param {String} 			evt
	 * @param {Function} 		cb
	 */
	util.evt.on = function(obj, evt, cb) {
		if (obj instanceof EventTarget) {
			obj.addEventListener(evt, cb);
		}
	}

	/**
	 * Adds one time event listener.
	 *
	 * @param {EventTarget} 	obj 	An object that implements EventTarget interface
	 * @param {String} 			evt
	 * @param {Function} 		cb
	 */
	util.evt.one = function(obj, evt, cb) {
		if (obj instanceof EventTarget) {
			var handler = function(e) {
				cb.apply(null, arguments);
				e.target.removeEventListener(e.type, handler);
			};
			obj.addEventListener(evt, handler);
		}
	}



	util.obj.merge = function(obj1, obj2) {
		for (var prop in obj2) {
			if (obj2.hasOwnProperty(prop)) {
				if (typeof obj1[prop] === 'object' && typeof obj2[prop] === 'object') {
					app.util.obj.merge(obj1[prop], obj2[prop])
				} else {
					obj1[prop] = obj2[prop];
				}
			}
		}
	};

	/**
	 * Does not support arrays.
	 *
	 * Example:
	 * app.filter.ele = 'aaa';
	 * getPropVal('app.filter.ele'); 	// 'aaa'
	 * getPropVal('filter.ele', app); 	// 'aaa'
	 *
	 * @param {String} query 		Chain of properties
	 * @param {Object} [obj] 		Object to search in
	 * @return {mixed} 				The property value
	 * @throws {Error} 				If the parent of the property does not exist
	 */
	util.obj.getPropVal = function(query, obj) {
		var props 	= query.split('.');
		var curPropV= obj;
		var i 		= 0;

		if (curPropV == null) {
			curPropV = window[props[0]];
			i++;
		}

		for (i; i < props.length; i++) {
			if (curPropV instanceof Object) {
				curPropV = curPropV[props[i]];
			} else {
				throw new Error('Cannot read property of undefined.');
				return;
			}
		}

		return curPropV;
	}

	/**
	 * Does not support arrays.
	 *
	 * @param {String} query 			Chain of properties
	 * @param {mixed} val 				Value to set
	 * @param {Object} [obj] 			Object to search in
	 * @return {void}
	 * @throws {Error} 					If the parent of the property does not exist
	 */
	util.obj.setPropVal = function(query, val, obj) {
		var props 		= query.split('.');
		var curProp 	= obj;
		var i 			= 0;

		if (curProp == null) {
			curProp = window[props[0]];
			i++;
		}

		for (i; i < props.length -1; i++) {
			var pName = props[i];

			if (curProp instanceof Object) {
				curProp = curProp[pName];
			} else {
				throw new Error('Cannot read property of undefined.');
				return;
			}
		}

		curProp[props[i]] = val;
	}



	util.str.nToBr = function (str) {
		if (str == undefined) {
			return null;
		}
		return str.replace(/\n/g, '<br>')
	}



	util.time.strToMs = function(str) {
		if (str == undefined) {
			return null;
		}

		var arr = str.split(/[:,]/);
		arr.reverse();

		var ms 	= arr[0] || '000';
		var sec = parseInt(arr[1] || 0);
		var min = parseInt(arr[2] || 0);
		var hour = parseInt(arr[3] || 0);

		// normalize milliseconds
		if (ms.length === 1) {
			ms += '00';
		} else if (ms.length === 2) {
			ms += '0'
		}
		ms = parseInt(ms);

		return hour*60*60*1000 + min*60*1000 + sec*1000 + ms;
	}

	util.time.msToStr = function(m) {
		if (m == undefined) {
			return null;
		}

		var ms 	= m % 1000;
		m = parseInt(m / 1000);
		var sec = m % 60;
		m = parseInt(m / 60);
		var min = m % 60;
		m = parseInt(m / 60);
		var hour = m;

		// normalize to strings
		// add zeros to the beginning
		if (ms < 10) {
			ms = '00' + ms;
		}
		else if (ms < 100) {
			ms = '0' + ms;
		}
		if (sec < 10) {
			sec = '0' + sec;
		}
		if (min < 10) {
			min = '0' + min;
		}
		if (hour < 10) {
			hour = '0' + hour;
		}

		// trim ms
		ms += '';
		if (ms[2] == '0') {
			ms = ms.slice(0, 2);
			if (ms[1] == '0') {
				ms = ms.slice(0, 1);
			}
		}

		var str = '';
		if (hour === '00') {
			if (min === '00') {
				str = sec + ',' + ms;
			} else {
				str = min + ':' + sec + ',' + ms;
			}
		} else {
			str = hour + ':' + min + ':' + sec + ',' + ms;
		}

		return str;
	}



	/**
	 * Single element view-to-model update.
	 * See get/setViewValue() for Element type compatibility.
	 * Calls v2mSetter function if present.
	 *
	 * @param {Element} [el] 	The view element of witch model should be updated
	 */
	util.mvc.updateModel = function(el) {
		var model 	= el.dataset.model;
		var setter 	= el.dataset.v2mSetter;
		var val 	= util.mvc.getViewValue(el);

		if (setter) {
			setter 	= util.obj.getPropVal(setter, app.mvcV2MSetters);
			setter(val, el);
		} else {
			util.obj.setPropVal(model, val, app);
		}

		// set model value
	}

	/**
	 * Single element model-to-view update.
	 * See get/setViewValue() for Element type compatibility.
	 * Calls m2vSetter function if present.
	 *
	 * @prop {Element} container 	Parent DOM Element to search in. Or the corresponding view Element.
	 * @prop {String} [model] 		Model query path. If not set "container" prop. is considered the corresponding view Element.
	 */
	util.mvc.updateView = function(container, model) {
		var el, setter;

		if (!model) {
			el 		= container;
			model 	= el.dataset.model;
		} else {
			el 		= util.dom.qs(container, '[data-model="' + model + '"]');
		}

		// if setter should be used
		setter = el.dataset.m2vSetter;
		if (setter) {
			setter = util.obj.getPropVal(setter, app.mvcM2VSetters);
			setter(el);
		} else {
			var val = util.obj.getPropVal(model, app);
			util.mvc.setViewValue(el, val);
		}

	}

	/**
	 * Works with checkboxes, select lists, textareas, text, inputs.
	 *
	 * @return {mixed} View value
	 */
	util.mvc.getViewValue = function(el) {
		var val;

		if (el.nodeName === 'INPUT') {
			switch (el.type) {
				case 'text':
					val = el.value;
					break;
				case 'checkbox':
					val = el.checked;
					break;
				default:
					throw new Error('View Element not implemented in MVC');
			}
		} else if (
			el.nodeName === 'SELECT' ||
			el.nodeName === 'TEXTAREA'
		) {
			val = el.value;
		}

		return val;
	}

	/**
	 * Works with checkboxes, select lists, textareas, text, inputs.
	 *
	 * @return {void}
	 */
	util.mvc.setViewValue = function(el, val) {
		// set view value
		if (el.nodeName === 'INPUT') {
			switch (el.type) {
				case 'text':
					el.value = val || '';
					break;
				case 'checkbox':
					el.checked = val;
					break;
				default:
					throw new Error('View Element not implemented in MVC');
			}
		} else if (
			el.nodeName === 'SELECT' ||
			el.nodeName === 'TEXTAREA'
		) {
			el.value = val || '';
		}
	}

})(window.app)
