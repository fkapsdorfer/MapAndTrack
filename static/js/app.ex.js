/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license GPLv3
 * @file ...
 */

'use strict';

(function(app){

	var ex 		= app.ex;
	var ui 		= app.ui;


	ex.tooManyFiles = function() {
		ui.prompt('File Upload Exception', 'Too many files.', function(){
			window.location.reload();
		});

		throw Error('app.ex.tooManyFiles')
	}

	ex.invalidFile = function(code, fname, more) {
		var msg = 'The file <em>' + fname + '</em> ';

		switch (code) {
			case 'e':
				msg += 'has an invalid extension. Supported <em>.tl.json (track layout)</em>, ';
				msg += '<em>.tf.json (track fill)</em>, <em>.ts.json (track standalone)</em>.';
				break;
			case 's':
				msg += 'is too big. Max. size 1024 KiB, given ' + more + ' KiB.';
				break;
			case 't':
				msg += 'has an invalid MIME type. Supported <em>application/javascript</em>, given <em>' + more + '</em>.';
				break;
			case 'p':
				msg += 'has an (JSON) syntax error.';
				console.log(more);
				break;
		}
		ui.prompt('File Upload Exception', msg, function(){
			window.location.reload();
		});

		throw Error('app.ex.invalidFile');
	}

	ex.invalidFileVariation = function() {
		ui.prompt('Invalid File Variation', '<em>*.tf.json</em> file cannot be stand-alone.', function(){
			window.location.reload();
		});

		throw Error('app.ex.invalidFileVariation');
	}

	ex.downloadNotSupported = function(fextension, rawData) {
		ui.prompt(
			'Feature Not Supported By Your Browser',
			'Your browser does not support the technique this application uses for downloading. Upgrade your browser if you want to use this feature.',
			function() {
				ui.promptRawData('You can still copy and save this data. Use filename with this extension: <em>' + fextension + '</em>. <strong>The raw output:</strong>', rawData);
			}
		);
	}

})(window.app)
