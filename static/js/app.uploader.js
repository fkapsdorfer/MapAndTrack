/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2015
 * @license GPLv3
 * @file ...
 */

'use strict';

(function(app){

	var up 			= app.uploader;
	var ex 			= app.ex;
	var dom 		= app.dom;
	var ui 			= app.ui;
	var util 		= app.util;


	up.initUploader = function() {
		var u = dom.uploader;
		// d'n'd listeners
		u.addEventListener('dragover'	, up._handleFileDragOver);
		u.addEventListener('drop'		, up._handleFileDrop);
		// click listeners
		u.addEventListener('click' 		, function(){ dom.uploaderFile.click() });
		u.addEventListener('change'		, up._filesSelected);
	}

	up._handleFileDrop = function(evt) {
		evt.stopPropagation();
		evt.preventDefault();

		var files = evt.dataTransfer.files;

		if (files.length < 1) {
			return;
		}

		up._proccessNewFiles(files);
	}

	up._handleFileDragOver = function(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		evt.dataTransfer.dropEffect = 'copy';
	}
	up._filesSelected = function(evt) {
		var files = evt.target.files;

		// if no files (this shouldn't happen)
		if (files.length < 1) {
			return;
		}

		up._proccessNewFiles(files);
	}

	up._proccessNewFiles = function(files) {

		ui.blockUI(true);

		var data = {/*
			tl, 	// track layout
			lf, 	// track fill
			ts 		// track standalone
		*/};
		var maxSize 	= 1024*1024;
		var len 		= files.length;
		var readCount 	= 0;
		var readers 	= [];

		if (2 < len) {
			ex.tooManyFiles();
		}

		for (var i = 0; i < len; i++) {
			var file 		= files[i];
			var name 		= file.name;
			//var mimeOk 		= file.type === 'application/javascript' || file.type === '';	// will accept unknown
			var mimeOk 			= true;
			var sizeOk 		= file.size < maxSize;
			var type;
			readers[i] 		= new FileReader();

			// check type
			if (name.match(/\.tl\.json$/)) {
				type = 'tl';
			} else if (name.match(/\.tf\.json$/)) {
				type = 'tf';
			} else if (name.match(/\.ts\.json$/)) {
				type = 'ts';
			} else {
				ex.invalidFile('e', name);
			}
			// check size (1 MiB)
			if (!sizeOk) {
				ex.invalidFile('s', name, (file.size / 1024).toFixed(2));
			}
			// check MIME
			if (!mimeOk) {
				ex.invalidFile('t', name, file.type || 'unknow');
			}

			// listen
			util.evt.one(readers[i], 'load', (function(type){
				return function(evt) {
					try {
						data[type] = JSON.parse(evt.target.result);
					} catch (err) {
						ex.invalidFile('p', name, err);
					}

					readCount++;

					if (readCount === len) {
						// done
						app.import(data, len);
						ui.unblockUI();
					}
				}
			})(type));

			// read
			readers[i].readAsText(file);
		}
	}

})(
	typeof window.app === 'object'
		? window.app
		: window.app = {}
)
