
Vertex Interaction, Terminology, Types of Vertices
==================================================

Vertex States, Interaction
--------------------------
#### Selected Vertex
The vertex of which the data is being displayed in the *description widget*.

You can select a vertex by **clicking** (or tapping on mobile) it or hovering (*filters*) on it.

#### Current Vertex
Represents a vertex that would be edited.

You can set the *current vertex* by *clicking* (tapping) a vertex (same as *selecting* a vertex).

#### Locked Vertex
At the same time is considered as a *current vertex*.
All *selected vertices* will be compared relatively to the *locked vertex*.

You can lock a vertex in the ***filter widget*** by **ticking** the *lock vertex* check-box.


(Vertex) Milestones
-------------------
Milestones are vertices that have certain properties set.

#### Time Milestone
***Delta time*** is present.

#### Elevation Milestone
***Elevation*** is present.


Splits
------
A split is considered a track between two *time milestones*.
