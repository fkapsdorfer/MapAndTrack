
Export and Import
=================
1. You create tracks in this application.
2. You preserve the tracks by downloading (export) them and storing them in your computer.
3. You view the tracks again by uploading (inport) them into the application.


Export Types
------------
There are three different export types.

### Track Layout `*.tl.json`
Track layout contains a path itself. It can also include notes.

### Track Fill `*.tf.json`
Track fill contains your times and notes.
It cannot be imported stand-alone but with the corresponding *track layout*.
*Track fill* data has a higher priority than *track layout* data (e.g. notes in track layout will be overwriten).
This brings the advantage of having a single track layout and multiple track fills.

### Stand-alone Track `*.ts.json`
Stand-alone track is a combination of the both previously mentioned types.


What Do You Store
-----------------

### Track Layout `*.tl.json`
* `mapOpts`: always
* `track`:
	* `lat`, `lng`: always
	* `time`: if present
	* `ele`: if present
	* `note`: if present, include in the export options

```json
{
    "mapOpts":{
        "center":{
            "lat":48.9441036981388,
            "lng":20.514173656702038
        },
        "zoom":15,
        "mapTypeId":"ROADMAP"
    },
    "track":[
        {
            "lat":48.95272977983339,
            "lng":20.51018714904785,
			"time": 50000,
			"ele": 500,
            "note":"This is a note.<br>A new line."
        },
		...
    ]
}
```

### Track Fill `*.tf.json`
* `mapOpts`: optional - include in the export options
* `track`:
	* \* note the `track` property is an object not an array, so the vertices can be matched with `*.tl.json`.
	* `lat`, `lng`: no
	* `time`: if present
	* `ele`: if present
	* `note`: if present

```json
{
    "mapOpts":{
        "center":{
            "lat":48.9441036981388,
            "lng":20.514173656702038
        },
        "zoom":15,
        "mapTypeId":"ROADMAP"
    },
    "track":{
        "8": {
			"time": 50000,
			"ele": 500,
            "note":"This is a note.<br>A new line."
        },
		...
    }
}
```

### Stand-alone Track `*.ts.json`
Same as with ``*.tl.json` expect notes cannot be excluded by the export option.
