
Map & Track
===========
This is a non-server web application for tracking your sport activity such as hiking, running, inline, cycling, etc.
You can plot a route and attach some data to any point of the route.
Then you can analyze the activity (distance, time, speed, elevation, notes, i.a.).
The activity is exportable and importable.


Documentation:
--------------
You can find the user/development guide in the `/doc/markdown` directory.


License:
--------
The entire project is licensed under **GPLv3** (http://opensource.org/licenses/GPL-3.0) with **exception of vendor sources**.

The project uses the **Google Maps API** (no API key) which has a **custom a licensing program** (free for non-comercial, publicly available project; https://developers.google.com/maps/licensing).
Google Maps API is loaded from the Google servers.

The tests uses the **QUnit** testing framework, and **QUnit Close** assertion plug-in licensed under **MIT** (http://opensource.org/licenses/MIT).
Sources are found in the `/test/vendor` directory.
