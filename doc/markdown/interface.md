
Interface
=========

Build for mobile
----------------
The whole interface is designed to be fully functional on mobile devices of all sizes.
Everything is laid out the same way as on the desktop.
The touch screen interaction is similar to the mouse interaction and intuitive.
All you need is a modern browser.

Widgets
-------

### Map Canvas
You use it to create tracks and visualize them.

You **select vertices** you want to edit or display information in the *description box* by **clicking** or **taping** them.
This will also select the vertex as the *current vertex* if there's no *locked vertex*.

The **pin** denotes the vertex you are currently editing.
If the pin is labeled with "L" this means the vertex is also *locked* and other *selected vertices* will be compared against this one.

### Description Box / Widget
All useful instant information is shown here.
Data about track segments, *splits*, or vertices are displayed here.

With help of *filters* you can adjust the behavior or data being displayed.

The description box shows up after you *select* a vertex by **clicking** (or tapping on mobile) or hovering (*filters*).
The widget can be **hidden** by clicking on the map canvas (no new vertex will be created).

For more about displayed data see [Data Displayed](data-displayed.md).

### Dashboard
Dashboard is a wrapper for another widgets. It can be **pushed out** of the screen view and is **scrollable**.

#### Vertex Editor
With the vertex editor you can edit properties of the *currently selected* vertex.
The properties you can edit here are *delta time*, *elevation*, and *notes*.

\* delta - see [What Delta Denotes](data-displayed.md#what-delta-denotes)

#### Filter
There are different filter so you have the ability to change how the interface respond to your command.

#### Export
Here you can export your tracks. For more information see [Export and Import](export-and-import.md).

### Prompt
Prompt is used to inform about some important events or facts. The prompt blocks the whole interface until it is confirmed.

#### Raw Data Prompt
This type of prompt can contain a message and a raw data output (might be a URL for example). A concrete example is when the user's browser doesn't support *"in-browser"* downloading (export).


More About Map Interaction:
---------------------------
see [Vertex Interaction, Terminology, Types of Vertices](vertices.md)
