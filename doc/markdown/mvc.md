
MVC
===

`app.mvcM2VSetters` resp. `app.mvcV2MSetters`
---------------------------------------------
Are containers for functions used as setters between model-view binding vice versa.
If a setter is preset the value in view/model will be set by this function not by the default update functions (`util.mvc.updateView()` resp. `util.mvc.updateModel()`).
The setters are defined in the as data attributes - `data-m2v-setter`, `data-v2m-setter`.

Setters are useful when the input/output data needs to be filtered or processed.
They are also handy when a change to a model, or view affects some other things.

Those function doesn't have to return anything, they change the value alone.

#### Parameters Passed
```javascript
/**
 * @param {Element} el       The corresponding view element
 */
m2vSetter(el)
/**
 * @param {mixed}   value   The elements value value got by util.obj.getPropVal()
 * @param {Element} el      The corresponding view element
 */
v2mSetter(val, el)
```

#### Implementation Example
```javascript
app.mvcM2VSetters.vertexEditor = function(el) {
	var val;
	var i 			= app._currentVertex;
	var prop 		= el.dataset.model;
	var disable 	= i == null
		? true
		: false;

	val = i != null
		? app.data.track[i][prop] || ''
		: '';

	if (prop === 'timeDelta' && val !== '') {
		val = app.util.time.msToStr(val);
	}

	el.disabled = disable;
	el.value 	= val;
}
app.mvcV2MSetters.vertexLocked = function(val, el) {
	var res;
	if (val) {
		res = app._lockVertex();
	} else {
		res = app._unlockVertex();
	}
	// if unable to lock (if no current vertex set)
	if (!res) {
		app.util.mvc.updateView(el);
	}
}
```


View Data Attributes
--------------------
Model-view is not automatically binded by this attributes.
They just refer the view to the corresponding model (`data-model`),
and optionally say how the data should be treated (`data-m2v-setter`, `data-v2m-setter`).

By default (no setter) models are searched in the `app`.
Setter functions are searched in the `app.mvcM2VSetters` resp. `app.mvcV2MSetters`.
This behavior is defined in `util.mvc.updateView()` resp. `util.mvc.updateModel()`.

##### `data-model`
Refers to the corresponding model.

##### `data-m2v-setter`
Defines a path to a function that should be used to set a view value according to the model. This is useful for formating the data output (milliseconds to formated time).

##### `data-m2v-setter`
Defines a path to a function that should be used to set a model value according to the view. This is useful for formating the data input (formated time to milliseconds).


Binding
-------
There is just one way (kind of automatic) binding, that is view -> model (not vice versa).
The binding is constructed somewhere in the `app.ui` via some kind of event listener (of which handler internally uses `app.util.mvc.updateModel()`).

The model -> view is is done manually by every function that alter the value of a model element (using `app.util.mvc.updateView()`).

#### View-Model Binding Example
```javascript
var vEditors 	= dom.dashboard.vertexEditors;
// vertex editors - listeners
// iterating over vertex editor inputs (Element(s))
for (var i = 0; i < vEditors.length; i++) {
	var el = vEditors[i];
	// all elements are text inputs or textarea
	on(el, 'change', hdlr.changeVertexEditor);
}

// handler
hdlr.changeVertexEditor = function() {
	util.mvc.updateModel(this);
}
```
#### Model-View Update Example
``` javascript
function setA(val) {
	_setA(val);
	util.mvc.updateView(app.dom.a);
}
function _setA(val) {
	app.a = val;
}
```



(Model) Setter Functions
------------------------
Functions that alters a model value and does not update the view are prefixed with an underscore.
Those functions may have have their wrapping functions that internally call them plus update the view (using `app.util.mvc.updateView()`).

E.g.:
```javascript
app._unlockVertex = function() {
	// ...
}
app.unlockVertex = function() {
	app._unlockVertex();
	util.mvc.updateView(app.dom.dashboard.filter, '_vertexLocked');
}
```


MVC Related Functions
---------------------

```
{void} app.util.mvc.updateView({Element} container(/el), {String} [model])
{void} app.util.mvc.updateModel({Element} el)

{mixed} app.util.mvc.getViewVal({Element} el)
{mixed} app.util.mvc.setViewVal({Element} el)

{mixed} app.util.obj.getPropVal({String} query, {Object} obj);
{mixed} app.util.obj.getPropVal({String} query, {mixed} val, {Object} obj);
```

```javascript
/**
 * Single element model-to-view update.
 * See get/setViewValue() for Element type compatibility.
 * Calls m2vSetter function if present.
 *
 * @prop {Element} container    Parent DOM Element to search in. Or the corresponding view Element.
 * @prop {String} [model]       Model query path. If not set "container" prop. is considered the corresponding view Element.
 */
app.util.mvc.updateView(container, model);

/**
 * Single element view-to-model update.
 * See get/setViewValue() for Element type compatibility.
 * Calls v2mSetter function if present.
 *
 * @param {Element} [el]      The view element of witch model should be updated
 */
app.util.mvc.updateModel(el);


/**
 * Works with checkboxes, select lists, textareas, text, inputs.
 *
 * @return {mixed} View value
 */
app.util.mvc.getViewVal(el);

/**
 * @return {void}
 */
app.util.mvc.setViewVal(el);


/**
 * Does not support arrays.
 *
 * @param {String} query        Chain of properties
 * @param {mixed}  val          Value to set
 * @param {Object} [obj]        Object to search in
 * @return {void}
 * @throws {Error}              If the parent of the property does not exist
 */
app.util.obj.getPropVal(query, obj);
app.util.obj.getPropVal(query, val, obj);
```
![util.mvc.updateView()](../images/util-mvc-updateModel.svg)
![util.mvc.updateModel()](../images/util-mvc-updateView.svg)
