Data Displayed
==============

Vertex Data Displayed in the Description Widget
-----------------------------------------------
If you *select* a vertex the *description widget* show up. The **amount of data** displayed usually depends on the type of the selected *milestone* and a *milestone of relation*.

The ***milestone of relation*** (MoR) can be forced by *locking* a vertex. Otherwise it will be **automatically found** depending on the data property, see below how.

### What Delta Denotes
Deltas of *time*, *distance*, *elevation*, and *cumulative elevation* are values relative to the nearest previous *time milestone*.

### The Data, Defining Milestones of Relation
* `SV-C`    - a condition for the selected vertex  
* `MoR-D`   - a definition of the milestone of relation  
* `MoR-C`   - a condition for the milestone of relation  
* `Def`     - a definition of the data presented  
* `(vetex)` - the data is not relative to any other vertex  
* `...`     - self explanatory

#### No Locked Vertex Present

##### Latitude
* `SV-C`: 	None.  
* `MoR-D`: 	(vetex)  
* `MoR-C`: 	(vetex)  
* `Def`: 	...  
##### Longitude
* `SV-C`: 	None.  
* `MoR-D`: 	(vetex)  
* `MoR-C`: 	(vetex)  
* `Def`: 	...  
##### Distance (delta)
* `SV-C`: 	None.  
* `MoR-D`: 	The nearest previous time milestone.  
* `MoR-C`: 	Must be a time milestone.  
* `Def`: 	A distance between SV and MoR.  
##### Distance (total)
* `SV-C`: 	None.  
* `MoR-D`: 	All previous time milestones.  
* `MoR-C`: 	Must be time milestones.  
* `Def`: 	The sum of all previous delta distances.  
##### Time (delta)
* `SV-C`: 	Must be a time milestone.  
* `MoR-D`: 	The nearest previous time milestone.  
* `MoR-C`: 	Must be a time milestone.  
* `Def`: 	Time delta between SV and MoR.  
##### Time (total)
* `SV-C`: 	Must be a time milestone.  
* `MoR-D`: 	All previous time milestones.  
* `MoR-C`: 	Must be time milestones.  
* `Def`: 	The sum of all previous delta times.  
##### Avrg. speed
* `SV-C`: 	Must be a time milestone.  
* `MoR-D`: 	The nearest previous time milestone.  
* `MoR-C`: 	Must be a time milestone.  
* `Def`: 	...  
##### Elevation
* `SV-C`: 	Must be an elevation milestone.  
* `MoR-D`: 	(vetex)  
* `MoR-C`: 	(vetex)  
* `Def`: 	...  
##### Elevation (delta)
* `SV-C`: 	Must be an elevation milestone.  
* `MoR-D`: 	The nearest previous time milestone.  
* `MoR-C`: 	Must be an elevation milestone.  
* `Def`: 	Difference of elevations.  
##### Cumulative Elevation - Elevation (+), Elevation (-)
* `SV-C`: 	Must be an elevation milestone.  
* `MoR-D`: 	The previous time milestone up to the nearest time milestone including.  
* `MoR-C`: 	Must be elevation milestones, the last must be also a time milestone.  
* `Def`: 	The sum of cumulative ascending, and descending elevation.  
##### Note
* `SV-C`: 	Must have a note.  
* `MoR-D`: 	(vetex)  
* `MoR-C`: 	(vetex)  
* `Def`: 	...  

#### Locked Vertex
Same as previously mentioned, just replace the specified MoR(-D) with the locked vertex and keep the conditions (MoR-C).
